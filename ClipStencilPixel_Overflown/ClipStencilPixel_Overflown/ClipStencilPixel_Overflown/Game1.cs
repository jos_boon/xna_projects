using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ClipStencilPixel_Overflown
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // UI set up
        int drawIndex;
        KeyboardState prevKeyState;
        
        // Clipping
        Texture2D box;

        // Stencil buffer
        Texture2D behindMask;
        Texture2D mask;

        // PixelShader
        Texture2D colors;
        Texture2D IMG;
        Effect spriteEffect;

        // Car PixelShader
        Texture2D car;
        Texture2D carTexture;
        Effect carColorEffect;

        // Light Shader
        Texture2D lightMask;
        Effect lightingEffect;
        RenderTarget2D lightsTarget;
        RenderTarget2D mainTarget;

        // Default font
        SpriteFont font;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            drawIndex = 0;
            prevKeyState = Keyboard.GetState();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load in Textures
            box = Content.Load<Texture2D>("Cube");
            behindMask = Content.Load<Texture2D>("BehindTheMask");
            mask = Content.Load<Texture2D>("CircleMask");
            font = Content.Load<SpriteFont>("TextFont");
            colors = Content.Load<Texture2D>("Colors");
            IMG = Content.Load<Texture2D>("HD-landscape-Photographs");
            car = Content.Load<Texture2D>("car");
            carTexture = Content.Load<Texture2D>("rainbow");
            lightMask = Content.Load<Texture2D>("lightmask");

            // Load in PixelShaders
            spriteEffect = Content.Load<Effect>("SpriteEffect");
            carColorEffect = Content.Load<Effect>("CarColor");
            lightingEffect = Content.Load<Effect>("LightingEffect");

            // Setup for Lighting effects
            PresentationParameters pp = GraphicsDevice.PresentationParameters;
            lightsTarget = new RenderTarget2D(GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight);
            mainTarget = new RenderTarget2D(GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight);
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState curr = Keyboard.GetState();

            if (prevKeyState.IsKeyDown(Keys.Tab) && curr.IsKeyDown(Keys.Tab) == false)
                drawIndex = ++drawIndex % 5;

            prevKeyState = curr;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            if (drawIndex == 0)
                Clipping();
            else if (drawIndex == 1)
                StencilBuffering();
            else if (drawIndex == 2)
                PixelShader(gameTime);
            else if (drawIndex == 3)
                CarPixelShader(gameTime);
            else if (drawIndex == 4)
                LightEffects();

            spriteBatch.Begin();
            spriteBatch.DrawString(font, "Press Tab to toggle Draw methods.", Vector2.Zero, Color.Black);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void Clipping()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            RasterizerState rs = new RasterizerState() { ScissorTestEnable = true };
            Rectangle clip = new Rectangle(50, 100, 50, 50);
            Rectangle fullClip = GraphicsDevice.ScissorRectangle;

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, DepthStencilState.Default, rs);
            
            GraphicsDevice.ScissorRectangle = clip;
            spriteBatch.Draw(box, new Vector2(50, 100), Color.Red);

            clip = new Rectangle(100, 100, 50, 50);
            GraphicsDevice.ScissorRectangle = clip;
            spriteBatch.Draw(box, new Vector2(50, 100), Color.Blue);

            clip = new Rectangle(50, 150, 50, 50);
            GraphicsDevice.ScissorRectangle = clip;
            spriteBatch.Draw(box, new Vector2(50, 100), Color.Yellow);

            clip = new Rectangle(100, 150, 50, 50);
            GraphicsDevice.ScissorRectangle = clip;
            spriteBatch.Draw(box, new Vector2(50, 100), Color.Green);

            GraphicsDevice.ScissorRectangle = fullClip;
            spriteBatch.Draw(box, new Vector2(50, 210), null, Color.Green, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            spriteBatch.Draw(box, new Vector2(75, 210), null, Color.Blue, 0, Vector2.Zero, 1, SpriteEffects.None, 1);

            spriteBatch.End();
        }

        private void StencilBuffering()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            Matrix m = Matrix.CreateOrthographicOffCenter(0, GraphicsDevice.PresentationParameters.BackBufferWidth,
                        GraphicsDevice.PresentationParameters.BackBufferHeight, 0, 0, 1);
            AlphaTestEffect a = new AlphaTestEffect(GraphicsDevice);
            a.Projection = m;

            MouseState mState = Mouse.GetState();
            Vector2 pos = new Vector2(mState.X, mState.Y) - new Vector2(mask.Width / 2, mask.Height / 2);

            DepthStencilState s1 = new DepthStencilState() { StencilEnable = true, StencilFunction = CompareFunction.Always, StencilPass = StencilOperation.Replace, ReferenceStencil = 1, DepthBufferEnable = false };
            DepthStencilState s2 = new DepthStencilState() { StencilEnable = true, StencilFunction = CompareFunction.LessEqual, StencilPass = StencilOperation.Keep, ReferenceStencil = 1, DepthBufferEnable = false };
            
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, s1, null, a);
            spriteBatch.Draw(mask, pos, null, Color.White, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0.0f);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, s2, null, a);
            spriteBatch.Draw(behindMask, new Vector2(250, 100), Color.White);
            spriteBatch.DrawString(font, "Hello World!", new Vector2(250, 50), Color.White);
            spriteBatch.End();
        }

        private void PixelShader(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

            spriteEffect.CurrentTechnique.Passes[1].Apply();

            spriteEffect.Parameters["DesaturationAmount"].SetValue((float)gameTime.TotalGameTime.TotalSeconds);
            int finalHeight = (int)Math.Round(IMG.Height * .35f); // float is unreliable (floating point)
            spriteEffect.Parameters["ImageHeight"].SetValue(finalHeight);
            spriteEffect.Parameters["Contrast"].SetValue(0.35f);
            spriteEffect.Parameters["Brightness"].SetValue(-.2f);
            
            spriteBatch.Draw(IMG, new Vector2(15, 15), null, Color.White, 0, Vector2.Zero, .35f, SpriteEffects.None, 0);

            // Revert back to standard drawing
            spriteEffect.CurrentTechnique.Passes[0].Apply();

            spriteBatch.End();
        }

        private void CarPixelShader(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

            spriteBatch.Draw(car, new Vector2(100, 100), null, Color.White, 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0);

            carColorEffect.Parameters["iGlobalTime"].SetValue((float)gameTime.TotalGameTime.TotalSeconds);
            carColorEffect.Parameters["someTex"].SetValue(carTexture);
            carColorEffect.CurrentTechnique.Passes[0].Apply();
            spriteBatch.Draw(car, new Vector2(100, 200), null, Color.White, 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0);

            carColorEffect.CurrentTechnique.Passes[2].Apply();
            spriteBatch.Draw(car, new Vector2(100, 300), null, Color.White, 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0);

            spriteBatch.End();
        }
        
        private void LightEffects()
        {
            MouseState mState = Mouse.GetState();
            Vector2 pos = new Vector2(mState.X, mState.Y) - new Vector2(lightMask.Width / 2, lightMask.Height / 2);

            // Setting the render target allows us to create a new texture in real-time
            GraphicsDevice.SetRenderTarget(lightsTarget);
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
                spriteBatch.Draw(lightMask, pos, Color.White);
                spriteBatch.Draw(lightMask, new Vector2(50, 250), Color.White);
                spriteBatch.Draw(lightMask, new Vector2(200, 250), Color.White);
                spriteBatch.Draw(lightMask, new Vector2(350, 250), Color.White);
                spriteBatch.Draw(lightMask, new Vector2(550, 250), Color.White);
            spriteBatch.End();
          
            GraphicsDevice.SetRenderTarget(mainTarget);
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            spriteBatch.Begin();
                spriteBatch.Draw(box, new Vector2(50, 50), Color.Red);
                spriteBatch.Draw(box, new Vector2(600, 100), Color.Yellow);
                spriteBatch.Draw(box, new Vector2(100, 350), Color.Green);
                spriteBatch.Draw(box, new Vector2(600, 325), Color.Blue);
                spriteBatch.Draw(box, new Vector2(250, 120), Color.Purple);
            spriteBatch.End();

            // Resume drawing on the back buffer
            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                lightingEffect.Parameters["lightMask"].SetValue(lightsTarget);
                lightingEffect.CurrentTechnique.Passes[0].Apply();
                spriteBatch.Draw(mainTarget, Vector2.Zero, Color.White);
            spriteBatch.End();

        }
    }
}
