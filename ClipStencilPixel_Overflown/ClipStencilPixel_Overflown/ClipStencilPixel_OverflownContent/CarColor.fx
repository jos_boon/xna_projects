sampler s0;

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);

	if (!any(color))
		return color;

	float step = 1.0 / 7;

	// Feel the rainbow (if statements are not recommened!!)
	if (coords.x < (step * 1))
		color = float4(1, 0, 0, 1);
	else if (coords.x < (step * 2))
		color = float4(1, .5, 0, 1);
	else if (coords.x < (step * 3))
		color = float4(1, 1, 0, 1);
	else if (coords.x < (step * 4))
		color = float4(0, 1, 0, 1);
	else if (coords.x < (step * 5))
		color = float4(0, 0, 1, 1);
	else if (coords.x < (step * 6))
		color = float4(.3, 0, .8, 1);
	else
		color = float4(1, .8, 1, 1);

	// Gradiant effect
	color.rgb += coords.y;

	return saturate(color);
}

// Set parameter clip
float2 clip;
float4 ClipFunction(float2 coords : TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);
	if (coords.x > clip.x || coords.y > clip.y)
		color = float4(0, 0, 0, 0);

	return color;
}

// Vague name :D
texture someTex;
sampler someTex_sampler = sampler_state 
{ 
	Texture = someTex; 

	// Got an error that asked that the TextureAddressMode is set on clamp
	AddressU = Clamp;
	AddressV = Clamp;

};
float iGlobalTime;

float4 PassTexture(float2 coords : TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);

	float2 uv = coords;
	uv.x += iGlobalTime * .2;
	uv.x -= trunc(uv.x);

	float4 texCol = tex2D(someTex_sampler, uv);

	if (color.a)
		return texCol;

	return color;
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }

	pass Pass2
	{
		PixelShader = compile ps_2_0 ClipFunction();
	}

	pass Pass3
	{
		PixelShader = compile ps_2_0 PassTexture();
	}
}