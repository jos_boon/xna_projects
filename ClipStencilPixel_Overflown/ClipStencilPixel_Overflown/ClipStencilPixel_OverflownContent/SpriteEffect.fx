texture Texture;

sampler TextureSampler = sampler_state
{
	Texture = <Texture>;
};

struct VertexShaderOutput
{
	float4 Position : TEXCOORD0;
	float4 Color : COLOR0;
	float2 TextureCoordinate : TEXCOORD0;
    // TODO: add vertex shader outputs such as colors and texture
    // coordinates here. These values will automatically be interpolated
    // over the triangle, and provided as input to your pixel shader.
};

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TextureCoordinate);

    return color;
}

float4 InvertColor(VertexShaderOutput input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TextureCoordinate);

	if (color.a != 0)
	{
		color.rgb = 1 - color.rgb;
	}

	return color;
}

float4 Greyscale(VertexShaderOutput input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TextureCoordinate);
	float value = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;
	color.rgb = value;
	
	return color;
}

float4 Sepiatone(VertexShaderOutput input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TextureCoordinate);

	float3x3 sepia = { 0.393, 0.349, 0.272,
	0.769, 0.686, 0.534,
	0.189, 0.168, 0.131 };

	float4 result;
	result.rgb = mul(color.rgb, sepia);
	result.a = color.a;

	return result;
}

float4 DesaturationAmount;
float4 Desaturate(VertexShaderOutput input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TextureCoordinate);

	float desaturatedColor = dot(color, float3(0.3, 0.59, 0.11));
	color.rgb = lerp(color, desaturatedColor, sin(DesaturationAmount * .5));
	return color;
}

int ImageHeight;

float4 Scanlines(VertexShaderOutput input) : COLOR0
{
	float4 color;

	color = tex2D(TextureSampler, input.TextureCoordinate);
	int a = saturate((input.Position.y * ImageHeight) % 4);
	int b = saturate((input.Position.y * ImageHeight + 1) % 4);
	float m = min(a, b);
	
	color.rgb *= m;

	return color;
}

float Contrast;
float Brightness;

float4 BrightnessContrast(VertexShaderOutput input) : COLOR0
{
	float4 color = tex2D(TextureSampler, input.TextureCoordinate);

	float factor = (1.1 * (Contrast + 1.0)) / (1.0 * (1.1 - Contrast));
	float4 outcolor = factor * (color - 0.5) + 0.5;
	outcolor += Brightness;

	outcolor.a = color.a;
	return saturate(outcolor);
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }

	pass Pass2
	{
		PixelShader = compile ps_2_0 InvertColor();
	}

	pass Pass3
	{
		PixelShader = compile ps_2_0 Greyscale();
	}

	pass Pass4
	{
		PixelShader = compile ps_2_0 Sepiatone();
	}

	pass Pass5
	{
		PixelShader = compile ps_2_0 Desaturate();
	}

	pass Pass6
	{
		PixelShader = compile ps_2_0 Scanlines();
	}

	pass Pass7
	{
		PixelShader = compile ps_2_0 BrightnessContrast();
	}
}
